const gulp = require('gulp'),
sass = require('gulp-sass'),
browserSync = require('browser-sync'),
prefix = require('gulp-autoprefixer'),
concatcss = require('gulp-concat-css'),
runSequence = require('run-sequence'),
csso = require('gulp-csso');

gulp.task('server', function(){
    browserSync({
        notify: false,
        server:{
            baseDir: 'app'
        }
    });
});

gulp.task('style',function(){
    return gulp.src('app/styles/*.sass')
        .pipe(sass().on('error', sass.logError))
        .pipe(prefix({
            browsers:['last 15 versions']
        }))
        .pipe(gulp.dest('app/styles'))
        .pipe(browserSync.reload({stream: true}))
})

gulp.task('concat', function(){
    return gulp.src('app/styles/*.css')
        .pipe(concatcss('bundle.css'))
        .pipe(csso())
        .pipe(gulp.dest('app/dist'))
        .pipe(browserSync.reload({stream: true}))
})    

gulp.task('watch',['server','style','concat'], function(){
    gulp.watch(['app/styles/*.sass', 'app/styles/media.css'], function(){runSequence('style','concat')});
    gulp.watch('app/**/*.html').on('change', browserSync.reload);
    gulp.watch('app/**/*.js').on('change', browserSync.reload);
})