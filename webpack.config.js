var webpack = require('webpack');
var path = require('path');
var CopyWebpackPlugin = require('copy-webpack-plugin');
var ExtractTextPlugin = require("extract-text-webpack-plugin");
var HtmlWebpackPlugin = require('html-webpack-plugin');


module.exports = {
    context: path.resolve(__dirname, 'app'), // New line
    devServer: {
        contentBase: path.resolve(__dirname, 'dist'), // New line
    },
    module: {
        rules: [
            {
            test: /\.css$/,
            use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: [{loader: 'css-loader', options: {modules: true}},]
                }),
            },
            {
            test: /\.sass$/,
            use: ExtractTextPlugin.extract({
                    fallback: "style-loader",
                    use: ['css-loader',
                        {
                            loader: 'postcss-loader',
                            options: {
                                plugins: function () {
                                    return [require('autoprefixer')];
                                },
                            },
                        },
                        'sass-loader']
                })
            },
            {
            test: /\.(jpg|jpeg|png|svg)$/,
            use: [{
                loader: 'url-loader',
                query: {
                    useRelativePath: process.env.NODE_ENV === "production",
                    publicPath: process.env.NODE_ENV === "production" ? '/' : '/dist/'
                }
            }],
            },
        ],
    },
    entry: {
        index: './js/index.js'
    },
    output: {
        filename: '[name].bundle.js',
        path: path.resolve(__dirname, 'dist'), // Changed to dist/js/
        publicPath: process.env.NODE_ENV === 'production' ? null : 'dist/',
    },
    plugins: [
        new ExtractTextPlugin("styles.css"),
        new CopyWebpackPlugin([
            {from: 'assets/**/*'},
        ]),
        new HtmlWebpackPlugin({
            title: 'Sell It',
            template: 'index.html',
            filename: 'index.html',
            chunks: ['index']
        }),
        new HtmlWebpackPlugin({
            title: 'Details',
            template: 'single-page.html',
            filename: 'single-page.html',
            chunks: ['index']
        }),
        new HtmlWebpackPlugin({
            title: 'Welcome',
            template: 'login-page.html',
            filename: 'login-page.html',
            chunks: ['index']
        })
    ]
};