var main = $('.slider').slick({
    dots:false,
    prevArrow: false,
    nextArrow: '<div class="arrow-wrap"><img src="../assets/fa-long-arrow-right.png" class="arrow" alt=""></div>'
});

export default main